package com.example.expensetracker;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.firebase.Timestamp;

import java.util.Date;

public class RecordViewModel extends ViewModel {

    private final MutableLiveData<String> idItem = new MutableLiveData<>();
    private final MutableLiveData<String> descriptionItem = new MutableLiveData<>();
    private final MutableLiveData<Double> amountItem = new MutableLiveData<>();
    private final MutableLiveData<Date> dateItem = new MutableLiveData<>();
    private final MutableLiveData<Boolean> isIncomeItem = new MutableLiveData<>();

    public void setRecord(String id, String description, double amount, Date date, Boolean isIncome) {
        idItem.setValue(id);
        descriptionItem.setValue(description);
        amountItem.setValue(amount);
        dateItem.setValue(date);
        isIncomeItem.setValue(isIncome);
    }

    public String getIdItem() {
        return idItem.getValue();
    }

    public String getDescriptionItem() {
        return descriptionItem.getValue();
    }

    public Double getAmountItem() {
        return amountItem.getValue();
    }

    public Date getDateItem() {
        return dateItem.getValue();
    }

    public Boolean getIsIncomeItem() {
        return isIncomeItem.getValue();
    }

    public void unsetRecord() {
        idItem.setValue(null);
        descriptionItem.setValue(null);
        amountItem.setValue(null);
        dateItem.setValue(null);
        descriptionItem.setValue(null);
    }

}
