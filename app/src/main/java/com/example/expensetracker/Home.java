package com.example.expensetracker;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Home extends Fragment {

    ListView listView;
    RecordViewModel viewModel;
    Fragment fragment;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        FloatingActionButton fab = getActivity().findViewById(R.id.addExpense);
        fab.setVisibility(View.VISIBLE);

        View root = inflater.inflate(R.layout.home, container, false);

        viewModel = new ViewModelProvider(requireActivity()).get(RecordViewModel.class);
        this.fragment = this;

        return root;
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        FloatingActionButton addRecordButton = getActivity().findViewById(R.id.addExpense);
        addRecordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(Home.this)
                        .navigate(R.id.action_Home_to_AddRecord);

            }
        });


        listView = getView().findViewById(android.R.id.list);

        List<Map> data = fetchData();

        SwipeRefreshLayout swipeRefreshLayout = getView().findViewById(R.id.swiperefresh);

        swipeRefreshLayout.setOnRefreshListener(
            new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    fetchData();
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        );
    }

    public ArrayList<Map> fetchData() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        ArrayList<Map> result = new ArrayList<Map>();

        db.collection("records")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Map temp = document.getData();
                                temp.put("id", document.getId());
                                result.add(temp);
                            }
                        }


                        ListAdapter adapter = new ListAdapter(getContext(), result, viewModel, fragment);
                        listView.setAdapter(adapter);
                    }
                });

        return result;
    }

}