package com.example.expensetracker;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class ListAdapter extends ArrayAdapter<Map> {
    Context context;
    List<Map> data;

    private RecordViewModel viewModel;
    private Fragment fragment;

    ListAdapter (Context context, List<Map> data, RecordViewModel viewModel, Fragment fragment) {
        super(context, R.layout.row_record, data);

        this.context = context;
        this.data = data;
        this.viewModel = viewModel;

        this.fragment = fragment;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View row = convertView;
        RowViewHolder holder = null;
        if (row == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = layoutInflater.inflate(R.layout.row_record, parent, false);
            holder = new RowViewHolder(row);

            row.setTag(holder);
        }
        else {
            holder = (RowViewHolder) row.getTag();
        }

        Map record = data.get(position);

        holder.description.setText((String) record.get("description"));
        holder.amount.setText(record.get("amount").toString());

        if ((Boolean) record.get("income")) {
            holder.amount.setTextColor(ContextCompat.getColor(context, R.color.green));
        } else {
            holder.amount.setTextColor(ContextCompat.getColor(context, R.color.red));
        }

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteRecord((String) record.get("id"));
            }
        });
        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editRecord((String) record.get("id"), record);
            }
        });

        return row;

    }

    public void editRecord(String id, Map<String, Object> data) {
        String description = (String) data.get("description");
        double amount = (double) data.get("amount");
        Timestamp tempDate = (Timestamp) data.get("date");
        Date date = tempDate.toDate();
        Boolean isExpense = (Boolean) data.get("income");

        this.viewModel.setRecord(id, description, amount, date, isExpense);

        NavHostFragment.findNavController(fragment)
                .navigate(R.id.action_Home_to_AddRecord);
    }

    public void deleteRecord(String id) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        db.collection("records").document(id)
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(getContext(),"Registro eliminado", Toast.LENGTH_LONG).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getContext(),"Ocurrio un error el registro no fue eliminado", Toast.LENGTH_LONG).show();
                    }
                });
    }
}
