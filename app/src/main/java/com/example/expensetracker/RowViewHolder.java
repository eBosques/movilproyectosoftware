package com.example.expensetracker;

import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class RowViewHolder {

    TextView description;
    TextView amount;
    ImageButton delete;
    ImageButton edit;

    RowViewHolder(View v) {
        description  = v.findViewById(R.id.description_record);
        amount = v.findViewById(R.id.amount_record);
        delete = v.findViewById(R.id.deleteButton);
        edit = v.findViewById(R.id.editButton);
    }
}
