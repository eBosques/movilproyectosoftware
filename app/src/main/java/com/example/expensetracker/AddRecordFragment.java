package com.example.expensetracker;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class AddRecordFragment extends Fragment {

    RecordViewModel viewModel;
    String id;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.add_expense, container, false);

        EditText dateInput = root.findViewById(R.id.dateInput);

        dateInput.setInputType(InputType.TYPE_NULL);
        dateInput.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                {
                    InputMethodManager imm = (InputMethodManager) getContext().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                    showDateDialog(dateInput);
                }
            }
        });
        dateInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDateDialog(dateInput);
            }
        });

        viewModel = new ViewModelProvider(requireActivity()).get(RecordViewModel.class);
        this.id = null;

        return root;
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        FloatingActionButton fab = getActivity().findViewById(R.id.addExpense);
        fab.setVisibility(View.INVISIBLE);

        view.findViewById(R.id.cancelRecordButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(AddRecordFragment.this)
                        .navigate(R.id.action_AddRecord_to_Home);
            }
        });

        view.findViewById(R.id.addRecordButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (AddRecordFragment.this.id == null )
                    submitRecord();
                else
                    updateRecord();

                NavHostFragment.findNavController(AddRecordFragment.this)
                        .navigate(R.id.action_AddRecord_to_Home);
            }
        });

        fetchEdition();
    }

    public void submitRecord() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        Map<String, Object> record = readInput();

        db.collection("records")
                .add(record)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Toast.makeText(getContext(),"Registro guardado", Toast.LENGTH_LONG).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getContext(),"Ocurrio un error el registro no fue guardado", Toast.LENGTH_LONG).show();
                    }
                });
    }

    public void updateRecord() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        Map<String, Object> record = readInput();

        db.collection("records").document(this.id)
                .set(record)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(getContext(),"Registro actualizado", Toast.LENGTH_LONG).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getContext(),"Ocurrio un error el registro no fue actualizado", Toast.LENGTH_LONG).show();
                    }
                });
    }

    public Map<String, Object> readInput() {
        Map<String, Object> record = new HashMap<>();

        EditText descriptionInput = getView().findViewById(R.id.descriptionInput);
        EditText amountInput = getView().findViewById(R.id.amountInput);
        EditText dateInput = getView().findViewById(R.id.dateInput);
        CheckBox incomeInput = getView().findViewById(R.id.expenseCheckbox);

        String description = descriptionInput.getText().toString();

        String temp = amountInput.getText().toString();
        double amount = "".equals(temp) ? 0 : Double.parseDouble(temp);

        Date date;
        try {
            date = new SimpleDateFormat("dd/MM/yyyy").parse(dateInput.getText().toString());
        } catch (ParseException e) {
            date = new Date();
        }

        boolean isIncome = incomeInput.isChecked();

        record.put("description", description);
        record.put("amount", amount);
        record.put("date", new Timestamp(date));
        record.put("income", isIncome);

        return record;

    }

    public void showDateDialog(final EditText date) {
        final Calendar calendar=Calendar.getInstance();
        DatePickerDialog.OnDateSetListener dateSetListener=new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(Calendar.YEAR,year);
                calendar.set(Calendar.MONTH,month);
                calendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);
                SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd/MM/yyyy");
                date.setText(simpleDateFormat.format(calendar.getTime()));

            }
        };


        DatePickerDialog dialog = new DatePickerDialog(getContext(),dateSetListener,calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH));
        dialog.show();
    }

    public void fetchEdition() {
        this.id = viewModel.getIdItem();

        if (this.id != null) {
            String description = viewModel.getDescriptionItem();
            double amount = viewModel.getAmountItem();
            Date date = viewModel.getDateItem();
            Boolean isExpense = viewModel.getIsIncomeItem();

            setFields(description, amount, date, isExpense);
        }
    }

    public void setFields(String description, double amount, Date date, Boolean isExpense) {
        EditText descriptionInput = getView().findViewById(R.id.descriptionInput);
        EditText amountInput = getView().findViewById(R.id.amountInput);
        EditText dateInput = getView().findViewById(R.id.dateInput);
        CheckBox incomeInput = getView().findViewById(R.id.expenseCheckbox);

        descriptionInput.setText(description, TextView.BufferType.EDITABLE);
        amountInput.setText(Double.toString(amount), TextView.BufferType.EDITABLE);

        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd/MM/yyyy");
        String tempDate = simpleDateFormat.format(date.getTime());

        dateInput.setText(tempDate, TextView.BufferType.EDITABLE);
        incomeInput.setChecked(isExpense);

        viewModel.unsetRecord();
    }




}